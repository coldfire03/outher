#!/usr/bin/python3
import gi, sys, os

sampling = 100

if 2 > len(sys.argv):
    sys.stderr.write("Usage: {} [filename]\n".format(sys.argv[0]))
    sys.exit(1)

if not os.path.isfile(sys.argv[-1]):
    sys.stderr.write("File \"{}\" not found\n".format(sys.argv[-1]))
    sys.stderr.flush()
    sys.exit(1)
else:
    sys.stderr.write("Loading: {}\n".format(sys.argv[-1]))
    sys.stderr.flush()

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gdk, Gtk
from PIL import Image

image = Gtk.Image()
image.set_from_file(sys.argv[-1])
pb = image.get_pixbuf()
pb.savev("/tmp/image.jpeg","jpeg",[],[])
im = Image.open("/tmp/image.jpeg")
pix = im.load()
width, height = im.size
r = 0
g = 0
b = 0
for w in range(int(width/sampling)):
    for h in range(int(height/sampling)):
        rx,gx,bx = pix[w*sampling,h*sampling]
        r+=rx
        g+=gx
        b+=bx

def chex(c):
    if c < 16:
        return "0"+hex(c)[2:]
    else:
        return hex(c)[2:]

r = int(r*(sampling**2)/(width*height))
g = int(g*(sampling**2)/(width*height))
b = int(b*(sampling**2)/(width*height))

print("#{}{}{}".format(chex(r),chex(g),chex(b)))
