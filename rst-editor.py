from rst2html import rst2html
import gi
gi.require_version("WebKit2", "4.0")
gi.require_version('GtkSource', '4')

from gi.repository import WebKit2, Gtk, GLib, GtkSource, Pango

wnd = Gtk.Window()
box = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)
web = WebKit2.WebView()
rst = GtkSource.View()
lm = GtkSource.LanguageManager()
txt = ""
txtlast = ""

font = Pango.font_description_from_string("monospace")
rst.get_buffer().set_language(lm.get_language('rst'))
rst.modify_font(font)

def update():
    global txt
    global txtlast
    wbuffer = rst.get_buffer()
    txt = wbuffer.get_text(wbuffer.get_start_iter(), wbuffer.get_end_iter(),True)
    if txt != txtlast:
        txtlast = txt
        html, warning = rst2html(txt)
        web.load_html(html,"")
        GLib.timeout_add(200, update)
    GLib.timeout_add(1000, update)

wnd.connect("destroy", Gtk.main_quit)
scrolled_window = Gtk.ScrolledWindow()
scrolled_window.add(rst)
box.add1(scrolled_window)
box.add2(web)
box.set_position(400)
box.set_wide_handle(True)
wnd.add(box)
wnd.set_default_size(800, 600)
GLib.timeout_add(1000, update) 
wnd.show_all()
wnd.set_title("rst editor")
Gtk.main()
