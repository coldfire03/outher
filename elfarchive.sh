#!/bin/bash

list(){
    readelf -W -e "$1" | grep "] \." | cut -f2 -d"." | cut -f1 -d" " | grep "^file"
}
create(){
    file=/tmp/$RANDOM
    touch $file.c
    gcc -c $file.c -o "$1"
    rm -f $file.c
}

add(){
    file="$1"
    shift
    for arg in $@ ; do
        sym=$(echo $arg | base64 | sed "s/=/-/g")
        objcopy --add-section ".file$sym=$arg" $file
    done
}

del(){
    file="$1"
    shift
    for arg in $@ ; do
        sym=$(echo $arg | base64 | sed "s/=/-/g")
        objcopy -R.file"$sym" $file
    done
}

extract(){
    file="$1"
    shift
    for arg in $@ ; do
        sym=$(echo $arg | base64 | sed "s/=/-/g")
        mkdir -p $(dirname ./$arg)
        echo "Extract: $arg"
        objcopy --dump-section ".file$sym=./$arg" $file
    done
    if [[ "$@" == "" ]] ; then
       for arg in $(list "$file" | sed "s/^file//g;s/-/=/g" | base64 -d) ; do
           sym=$(echo $arg | base64 | sed "s/=/-/g")
           echo "Extract: $arg"
           objcopy --dump-section ".file$sym=$arg" $file
       done
    fi
}

if [[ "$1" == "c" ]] ; then
    create "$2"
elif [[ "$1" == "l" ]] ; then
    list "$2" | sed "s/^file//g;s/-/=/g" | base64 -d
elif [[ "$1" == "a" ]] ; then
    shift
    add $@
elif [[ "$1" == "d" ]] ; then
    shift
    del $@
elif [[ "$1" == "x" ]] ; then
    shift
    extract $@
else
    exec > /dev/stderr
    echo "Usage: archive [c/l/a/d/x] archive (file/files)"
    echo "  c : create new archive"
    echo "  a : add file/files to archive"
    echo "  d : delete file/files to archive"
    echo "  l : list files"
    echo "  x : extract file/files"
    exit 1
fi
