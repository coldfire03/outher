#!/usr/bin/python3
# simple notes application
import gi, os
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

Gtk.init()
# window init
window = Gtk.Window()
window.set_size_request(500,400)
# stack init
stack = Gtk.Stack()
stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
stack.set_transition_duration(300)
scrolled_window = Gtk.ScrolledWindow()
scrolled_window.add(stack)
window.add(scrolled_window)
# headerbar init
headerbar = Gtk.HeaderBar()
window.set_titlebar(headerbar)
label = Gtk.Label("Notes")
headerbar.pack_start(label)
new = Gtk.Button()
new.set_label("Add")
save = Gtk.Button()
save.set_label("Save")
delete = Gtk.Button()
delete.set_label("Delete")
headerbar.pack_end(new)
headerbar.pack_end(save)
headerbar.pack_end(delete)
# textview init
textview = Gtk.TextView()
textbuffer = textview.get_buffer()
textview.set_monospace(True)
textview.set_left_margin(3)
textview.set_right_margin(3)

cur_name = None
cur_button = None

buttons = []

def show_main():
    stack.set_visible_child_name("main")
    new.show()
    save.hide()
    delete.hide()
    update_buttons()
    
def show_edit():
    stack.set_visible_child_name("edit")
    save.show()
    delete.show()
    new.hide()
    update_buttons()

def update_buttons():
    for fname, b in buttons:
        if os.path.exists(fname):
            b.show()
        else:
            b.hide()

def note_button(name):
    global cur_name
    b = Gtk.Button()
    filename = "{}/.notes/{}.txt".format(os.environ["HOME"],name)
    if not os.path.exists(filename):
        f = open(filename,"w")
        f.close()
    f = open(filename,"r")
    b.add(Gtk.Label(f.readline()[:-1]))
    def action(widget):
        global cur_name
        global cur_button
        cur_button = b
        f = open("{}/.notes/{}.txt".format(os.environ["HOME"],name),"r")
        data = f.read()
        cur_name = name
        textbuffer.set_text(data)
        show_edit()
    b.connect("clicked",action)
    buttons.append((filename,b))
    return b

def save_action(widget):
    global cur_button
    if not  os.path.exists("{}/.notes/".format(os.environ["HOME"])):
        os.mkdir("{}/.notes/".format(os.environ["HOME"]))
    if cur_name == None or cur_button == None:
        print(cur_name,cur_button)
    f = open("{}/.notes/{}.txt".format(os.environ["HOME"],cur_name),"w")
    startIter, endIter = textbuffer.get_bounds()
    data = str(textbuffer.get_text(startIter, endIter,False))
    f.write(data)
    f.close()
    if cur_button:
        cur_button.set_label(data.split("\n")[0])
    show_main()


def delete_action(widget):
    global cur_button
    if cur_button:
        os.unlink("{}/.notes/{}.txt".format(os.environ["HOME"],cur_name))
        cur_button = None
    show_main()

def new_action(widget):
    global cur_name
    global cur_button
    cur_name = str(time.time())
    cur_button = note_button(cur_name)
    cur_button.show_all()
    main.add(cur_button)
    show_edit()
    textbuffer.set_text("")

main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
for note in os.listdir("{}/.notes/".format(os.environ["HOME"])):
    if note.endswith(".txt"):
        note = note[:-4]
        main.add(note_button(note))
        stack.set_visible_child_name("edit")

stack.add_titled(main, "main", "Main")
stack.add_titled(textview, "edit", "Edit")

save.connect("clicked",save_action)
delete.connect("clicked",delete_action)
new.connect("clicked",new_action)

window.show_all()
show_main()
Gtk.main()
